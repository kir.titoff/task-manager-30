package ru.t1.ktitov.tm.api.service;

import ru.t1.ktitov.tm.api.component.ISaltProvider;
import org.jetbrains.annotations.NotNull;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

}
